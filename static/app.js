const app = require('express')()
const cors = require('cors')

app.use(cors())
app.get('/api/home', (req, res) => {
    res.send({
        msg: '这是跨域数据'
    })
})

app.listen(3001)