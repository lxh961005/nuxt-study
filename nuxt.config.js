export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {

    title: process.env.npm_package_name || 'nuxt',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    './assets/css/reset.css',
    './assets/css/transition.css',
    'element-ui/lib/theme-chalk/index.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/router',
    {
      src: '~/plugins/axios',
      ssr: true // 服务端渲染
    },
    {
      src: '~/plugins/element-ui',
      ssr: true, // 不支持ssr的插件只会在客户端运行，不要给true
      // mode: 'server', // client
    }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxt/typescript-build'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    '@nuxtjs/style-resources'
  ],
  axios: {
    proxy: true, // 开启axios跨域
    prefix: '', // baseUrl
  },

  proxy: {
    '/api': {
      target: 'http://localhost:3001', // 代理转发的地址
      changeOrigin: true,
      // pathRewrite: {
      //   '^/api': '/api'
      // }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/]
  },
  router: {
    middleware: 'auth',
    // 扩展路由
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'root',
        path: '/index',
        component: resolve(__dirname, 'pages/index.vue')
      })
    }
  },
  /**
   * 自定义系统loading效果，或者指定一loading组件
   */
  // loading: {color: '#399',height: '3px'},
  loading: '~/components/loading.vue',
  styleResouces: {
    scss: [
      './assets/scss/global.scss'
    ]
  }
}
