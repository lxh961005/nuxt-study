export default function ({ $axios, redirct, route, store, app: { $cookies } }) {

    // 基本配置

    $axios.defaults.timeout = 100000;

    // 请求拦截
    $axios.onRequest(config => {
        console.log('请求拦截')

        // config.header.tokens = store.state.user.token
        return config
    })



    // 响应拦截
    $axios.onResponse(res => {
        console.log(res.data, '响应拦截')

        if (res.data.error === 2 && route.fullPath !== '/login') {
            redirct('/login?path=' + route.fullPath)
        }
        return res
    })
    // 错误处理

    $axios.onError(error => {
        return error
    })
}