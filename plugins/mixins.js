// 定义全局方法


// 定义全局filters

// 定义全局组件


// 定义全局指令



// 混入
Vue.mixin({
    methods: {
        $seo(title, content, payload = []) {
            return {
                title,
                meta: [{
                    hid: 'description',
                    name: 'keywords',
                    content,
                }].concat(payload)
            }
        }
    }
})