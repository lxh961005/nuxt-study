export const state = () => ({
    err: 1,
    data: {}
})

export const mutations = {
    M_UPDATE_HOME(state, playload) {
        state.err = playload.err;
        state.data = playload.data
    }
}


export const actions = {
    A_UPDATE_HOME({ commit, state }, payload) {
        // 异步处理
        commit('M_UPDATE_HOME', {err: 0,data: {title: 'home 模块，actions所传递的数据'}})

    }
}


export const getters = {

}

