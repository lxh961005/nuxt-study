// 主模块

// state 返回一个函数
export const state = () => ({
    bNav: false,
    bLoading: false
})

// mutations 对象
export const mutations = {
    M_UPDATE_NAV(state, payload) {
        state.bNav = payload
    },
    M_UPDATE_LOADING(state, playload) {
        state.bLoading = payload
    }
}

// actions
export const actions = {
    nuxtServerInit(store, { app: { $cookies } }) {
        // 初始话cookies
        let user = $cookies.get('user') ? $cookies.get('user') : { err: 2, msg: '未登录', token: '' }
        store.commit('user/M_UPDATE_USER', user)
        // store  vuex store
        // context
        // console.log('nuxtServerInit',)
    }
}


export const getters = {
    getNav(state) {
        return state.bNav ? '显示' : '隐藏'
    }
}