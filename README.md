### nuxt.config.js

- ​ head 配置<head>信息

- css 全局样式， Array<FilePath>

- router

  - middleware String 中间件
  - extendRoutes (routes,reolsve) 路由列表，path.resolve

- modules 添加@nuxtjs 下模块

- plugins 添加一个文件地址， 该文件导出一个函数，这个函数自带上下文内容

- loading: 组件 loading 效果

  - loading: {color: '#399',height: '3px'}, // 直接设置样式

  - loading: '~/components/loading.vue' // 指向组件路径, 组件需要实现 start 和 finish 的事件

##### Layout

- default 布局组件
- error 错误组件

#### 生命周期

##### nuxtServerInit

store 操作适用场景是对 store 的操作

##### middleware

中间执行流程顺序：nuxt.config.js -> 匹配布局 -> 匹配页面

##### validate

参数校验 校验失败，则自动跳转错误页面

##### asyncData

异步业务逻辑，读取服务端数据，返回给组件

##### fetch

异步业务逻辑，读取服务端数据提交给 vuex

#### SSR && CSR

- beforeCreate

- created

##### **CSR**

- beforeMount
- mounted
- beforeUpdate
- updated
- beforeDestroy
- destroyed
- activated
- deactivated

### 路由

```vue
<!-- name对应pages下目录结构，uid动态路由，query参数 uid为/goods/comment/_uid.vue -->
<!-- 
         声明式跳转  name： 路由名  目录名-其他目录-文件名 params: key 要对等文件名
 			子路由：目录代表子路由，子路由内部统计的文件，代表的是同级一级路由
         -->
<nuxt-link
  :to="{
    name: 'goods-comment-uid',
    params: { uid: 1 },
    query: { a: 1, b: 2 }
  }">评论01</nuxt-link>
<nuxt-link to="/goods/comment/2?a=11&b=22">评论02</nuxt-link>
<!-- 路由展示组件 -->
<nuxt />
```

#### 展示区层级控制

##### 注：goods.vue -> goods/index.vue 会将包含的效果变成替换，如果目录下没有 index.vue 会默认查找一个文件显示

layout/error.vue 错误时渲染组件， props 接受到 error 属性

#### 路由守卫

前置

- 依赖中间件 middleware 插件
- 全局守卫： nuxt.config 指向 middleware
- layouts 定义中间件
- 组件独享守卫： middleware
- 插件全局守卫

后置

- 使用 vue 的 beforeRouteLeave 钩子

### 数据交互

安装：

@nuxtjs/axios @nuxtjs/proxy

1. 可以直接在上下文中拿到\$axios
2. 可以在 nuxt.config.js 配置 axios 以及 proxy 的属性

#### Vuex

模块方式: 'store'目录下的每个'.js'文件会被转成为状态树, (当然 `index`是根模块),即每个 JS 都会被注册成一个新 module user.js user/A_UPDATE_USER

Classic(不建议使用): store/index.js 返回创建 Vuex.Store 实例的方法

注：每个模块的 state 都需要返回一个函数，state,actions,getters,mutations,actions 都需要对外导出

辅助函数没有使用变化

```javascript
// 主模块

// state 返回一个函数
export const state = () => ({
  bNav: false,
  bLoading: false
});

// mutations 对象
export const mutations = {
  M_UPDATE_NAV(state, payload) {
    state.bNav = payload;
  },
  M_UPDATE_LOADING(state, playload) {
    state.bLoading = payload;
  }
};

// actions
export const actions = {
  nuxtServerInit(store, context) {
    // store  vuex store
    // context
    // console.log('nuxtServerInit',)
  }
};

export const getters = {
  getNav(state) {
    return state.bNav ? "显示" : "隐藏";
  }
};
```

状态持久化 && token 校验(cookie-universal-nuxt)

​ 思想： 登录时，同步 vuex&& cookie，强制刷新后，nuxtServerInit 钩子，取出 cookies, 同步 vuex，axios 拦截器读取 vuex

#### meta 信息注入

- nuxt.config.js head
- 组件内部 head 函数返回一个对象

#### 引入 scss

1.  安装 node-sass sass-loader @nuxtjs/style-resources
2.  nuxt.config.js 配置 modules, 然后 styleResouces 设置文件路径

#### 资源指向与引入

- 相对路径指向需要压缩的资源
- 绝对路径找到无需压缩的资源

```javascript
  <!-- 相对路径找到需要压缩  assets -->
        <img src="../assets/btns.png" alt="">
        <!-- 绝对路径找到不需要压缩的  static/img/bg.jpg -->
        <img src="/img/bg.jpg" alt="">
```
